Equipment Required
==================

ASA firewall: running ASA 9.2.4
IOS router: ASR1004 with RP1 and IOS XE 3S. Should work with C819 as well.

Abstract Routing
================

Configure ASA to have a public routable IP address (or behind static NAT with TCP+UDP 443) with Nameif "internet" and security level 0.

Configure ASA to have a private IP address with Nameif "internal" and security level 100.

Configure ASR to have the following VRFs:

- BGP: Domestic BGP transit
- INTL: International transit
- MIXED-NAT: Mixed NAT vrf: domestic traffic will be sent to BGP vrf, default route will be sent to INTL vrf

IP SLA
======

Carefully measure typical latency and jitter before making decision on the SLA threshold. Keep in mind that when fully ultilized, increased latency will be reflected in SLA measurement.

Once you have the idea about the latency pattern, configure tracking and multiple `ip route`s (with different metric) to automatically failover to backup link when preferred link is down or is too congested.

If you are using MSTP/IPLC link, then you probably want to use EIGRP instead.

Special note
============

Please, avoid to use ASA as a router at all cost. It's a VPN headend.

Secure LDAP
===========

Well, it's complicated. You need to run stunnel on a few machines (for redundency) to convert SLDAP to LDAP. Use firewall to limit exposure.
